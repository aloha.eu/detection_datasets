import xml.etree.ElementTree as et
import os
import json
from utils import *
from uuid import uuid4
import shutil
import random
import numpy as np

VOC_CLASSES=["aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car",
             "cat", "chair", "cow", "diningtable", "dog", "horse", "motorbike",
             "person", "pottedplant", "sheep", "sofa", "train", "tvmonitor"]

catNms=VOC_CLASSES

# ANCHORS parameters
K = 5 # num of anchors
W, H = 416, 416
DIV = 32

MAX_SAMPLES = 1000 # the maximum size of the dataset

BALANCE = True
balance_tollerance = 30 
max_iterations = 3

train = 0.65
valid = 0.25
DATASET_SPLIT = [train, valid, 1-(train+valid)]

src_images_folder = "/home/gderiu/detection_datasets/VOC2012/JPEGImages"

if __name__ == "__main__":

  folder_path = "/home/gderiu/detection_datasets/VOC2012/Annotations"
  file_list  = os.listdir(folder_path)
  classes = '_'.join(catNms)
  dataset_folder = "VOCPASCAL_extract_{}_{}".format(str(uuid4()).split("-")[0], classes)
  dst_images_folder = os.path.join("./datasets", dataset_folder, "images")

  dataset = {}
  dataset["source_path"] = "images/"
  frames = []
  anns_for_anchors = []
  classes_count ={}
  for c in range(len(catNms)):
    classes_count[c]=0
    
  for f in file_list:
    tree = et.parse(os.path.join(folder_path,f))
    root = tree.getroot()
    image={}
    for c in root:
      image['objects']=[]
      image['classes_count']={}
      for i, cla in enumerate(catNms):
        image['classes_count'][i]=0
      if c.tag=="filename":
        image['frame']=c.text
    for c in root:
      if c.tag=='size':
        for c1 in c:
          if c1.tag=="width":
            image['width']=int(c1.text)
          if c1.tag=="height":
            image['height']=int(c1.text)
          if c1.tag=="depth":
            image['depth']=int(c1.text)
    for c in root:
      if c.tag=='object':
        obj ={}
        for c1 in c:
          if c1.tag=="name":
            if c1.text in catNms:
              obj['object_class']=catNms.index(c1.text)
              classes_count[obj['object_class']]+=1
              image['classes_count'][obj['object_class']]+=1
            else:
              break
          if c1.tag=="bndbox":

            for c2 in c1:
              if c2.tag=="xmin":
                x1 = float(c2.text)
              if c2.tag=="ymin":
                y1 = float(c2.text)
              if c2.tag=="xmax":
                x2 = float(c2.text)
              if c2.tag=="ymax":
                y2 = float(c2.text)
                
            obj['bb'] = [x1,y1,x2,y2]
            bb = convert_voc_bbox((image['width'], image['height']), obj['bb'])
            anns_for_anchors.append(bb[2:])
        if len(obj)>0:
          image['objects'].append(obj)
    if len(image['objects'])>0:
      frames.append(image)
  
  random.shuffle(frames)
  
  if BALANCE:
    import statistics
    d=statistics.pstdev(classes_count.values())

#    max_num= max(classes_count.values())
#    print('max_num')
#    print(max_num)
#    min_num= min(classes_count.values())
#    print('min_num')
#    print(min_num)
#    ratio = 1-min_num/max_num
    print ("StdDev: {}".format(d))
    print ("Tollerance stdDev: {}".format(balance_tollerance))
    print ("Total frames: {}".format(len(frames)))
    
    
    #if ratio>balance_tollerance:
    balanced_frames = []
    classes_count ={}
    for c in range(len(catNms)):
      classes_count[c]=0
    remaining = [fr['frame'] for fr in frames]
    for it in range(max_iterations):
      print ("\n\n\nLoop {}".format(it+1))
      for fr in frames:
#        import IPython
#        IPython.embed()
        if fr['frame'] not in remaining:
          continue
        if min(fr['classes_count'].values())>0:
          balanced_frames.append(fr)
          for c in fr['classes_count']:
            classes_count[c]+= fr['classes_count'][c]
          remaining.remove(fr['frame'])
        else:
          skip = False
          for c in fr['classes_count']:
            temp_classes_count = dict(classes_count)
            for c in fr['classes_count']:
               temp_classes_count[c]+= fr['classes_count'][c]
               
            if statistics.pstdev(temp_classes_count.values())>balance_tollerance:
              skip = True
              
        if skip == False:
          balanced_frames.append(fr)
          for c in fr['classes_count']:
            classes_count[c]+= fr['classes_count'][c]
          remaining.remove(fr['frame'])
            

         
      d=statistics.pstdev(classes_count.values())

      
  #    max_num= max(classes_count.values())
  #    print('max_num')
  #    print(max_num)
  #    min_num= min(classes_count.values())
  #    print('min_num')
  #    print(min_num)
  #    ratio = 1-min_num/max_num
      print ("StdDev: {}".format(d))
      print ("Tollerance stdDev: {}".format(balance_tollerance))
      print ("Total frames: {}".format(len(frames)))
    
    frames = balanced_frames
        
      
  random.shuffle(frames)
  frames = frames [:MAX_SAMPLES]
      
  dataset['meta']=frames
  
  classes_count = {}
  for c in range(len(catNms)):
    classes_count[c]=0
  for f in frames:
    for c in f['classes_count']:
      classes_count[c]+= f['classes_count'][c]


  anns_for_anchors = np.array(anns_for_anchors)
  
  centroids = anns_for_anchors[np.random.choice(np.arange(len(anns_for_anchors)), K, replace=False)]
  #centroids = anns_for_anchors[:K]
  coco_anchors = kmeans_iou(K, centroids, anns_for_anchors, feature_size= W / DIV)
  
  ind = random.sample([i for i in range(len(frames))], len(frames))
  
  ind_train = ind[ : int(len(frames)* DATASET_SPLIT[0])]
  ind_valid = ind[int(len(frames) * DATASET_SPLIT[0]) : int(len(frames)*(DATASET_SPLIT[0]+DATASET_SPLIT[1]))]
  ind_test  = ind[int(len(frames) * (DATASET_SPLIT[0]+DATASET_SPLIT[1])) : ]
  
  slices = [ind_train, ind_valid, ind_test]
  
  classes_count_tr = {}
  for c in range(len(catNms)):
    classes_count_tr[c]=0
  for i in ind_train:
    f = frames[i]
    for c in f['classes_count']:
      classes_count_tr[c]+= f['classes_count'][c]
  
  classes_count_v = {}
  for c in range(len(catNms)):
    classes_count_v[c]=0
  for i in ind_valid:
    f = frames[i]
    for c in f['classes_count']:
      classes_count_v[c]+= f['classes_count'][c]
  
  classes_count_te = {}
  for c in range(len(catNms)):
    classes_count_te[c]=0
  for i in ind_test:
    f = frames[i]
    for c in f['classes_count']:
      classes_count_te[c]+= f['classes_count'][c]

  print ("\nSaving images...")

  os.makedirs(dst_images_folder, exist_ok=True)
  for i in dataset['meta']:
    shutil.copy(os.path.join(src_images_folder,i['frame']), os.path.join(dst_images_folder,i['frame']))

  with open(os.path.join("./datasets", dataset_folder,"annotations.json"), "w") as text_file:
      text_file.write(json.dumps(dataset,indent=2, sort_keys=True))

  with open(os.path.join("./datasets", dataset_folder,"class_names.txt"), "w") as text_file:
    classes = ["{}\n".format(c) for c in catNms]
    text_file.write("".join(classes))
    
  print_anchors(coco_anchors, dataset_folder, W, H, DIV)
  with open(os.path.join("./datasets", dataset_folder,"anchors.txt"), "w") as text_file:
      ca = coco_anchors.tolist()
      for l in ca:
        text_file.write(str(l[0]) + ', ' + str(l[1]) + '\n')
        
  with open(os.path.join("./datasets", dataset_folder,"train_valid_split.json"), 'w') as f:
    dict_split = {'train_valid_split':slices}
    f.write(json.dumps(dict_split,indent=2, sort_keys=True))
         
  print ("\n\nFound {} images correspondig to categories: ".format(len(frames)))
  print (catNms)
  print ("classes_count full dataset")
  print (classes_count)
  print ("classes_count train split")
  print (classes_count_tr)
  print ("classes_count valid split")
  print (classes_count_v)
  print ("classes_count test split")
  print (classes_count_te)

  print ("Dataset folder: ", os.path.join("./datasets", dataset_folder))
