import numpy as np
from PIL import Image, ImageDraw, ImageFont
import os

def convert_coco_bbox(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    x = box[0] + box[2] / 2.0
    y = box[1] + box[3] / 2.0
    w = box[2]
    h = box[3]
    x = x * dw
    w = w * dw
    y = y * dh
    h = h * dh
    return x, y, w, h

def convert_voc_bbox(size, box):
    dw = 1. / size[0]
    dh = 1. / size[1]
    
    x = box[0] * dw
    y = box[1] * dh
    w = (box[2] - box[0])* dw
    h = (box[3] - box[1])* dh
    return x, y, w, h


def area(x):
    if len(x.shape) == 1:
        return x[0] * x[1]
    else:
        return x[:, 0] * x[:, 1]


def kmeans_iou(k, centroids, points, iter_count=0, iteration_cutoff=25, feature_size=13):
   # https://gist.github.com/WillieMaddox/3b1159baecb809b5fcb3a6154bc3cb0b
    best_clusters = []
    best_avg_iou = 0
    best_avg_iou_iteration = 0

    npoi = points.shape[0]
    area_p = area(points)  # (npoi, 2) -> (npoi,)

    while True:
        cen2 = centroids.repeat(npoi, axis=0).reshape(k, npoi, 2)
        cdiff = points - cen2
        cidx = np.where(cdiff < 0)
        cen2[cidx] = points[cidx[1], cidx[2]]

        wh = cen2.prod(axis=2).T  # (k, npoi, 2) -> (npoi, k)
        dist = 1. - (wh / (area_p[:, np.newaxis] + area(centroids) - wh))  # -> (npoi, k)
        belongs_to_cluster = np.argmin(dist, axis=1)  # (npoi, k) -> (npoi,)
        clusters_niou = np.min(dist, axis=1)  # (npoi, k) -> (npoi,)
        clusters = [points[belongs_to_cluster == i] for i in range(k)]
        avg_iou = np.mean(1. - clusters_niou)
        if avg_iou > best_avg_iou:
            best_avg_iou = avg_iou
            best_clusters = clusters
            best_avg_iou_iteration = iter_count

        print("\nIteration {}".format(iter_count))
        print("Average iou to closest centroid = {}".format(avg_iou))
        print("Sum of all distances (cost) = {}".format(np.sum(clusters_niou)))

        new_centroids = np.array([np.mean(c, axis=0) for c in clusters])
        isect = np.prod(np.min(np.asarray([centroids, new_centroids]), axis=0), axis=1)
        aa1 = np.prod(centroids, axis=1)
        aa2 = np.prod(new_centroids, axis=1)
        shifts = 1 - isect / (aa1 + aa2 - isect)

        # for i, s in enumerate(shifts):
        #     print("{}: Cluster size: {}, Centroid distance shift: {}".format(i, len(clusters[i]), s))

        if sum(shifts) == 0 or iter_count >= best_avg_iou_iteration + iteration_cutoff:
            break

        centroids = new_centroids
        iter_count += 1

    # Get anchor boxes from best clusters
    anchors = np.asarray([np.mean(cluster, axis=0) for cluster in best_clusters])
    anchors = anchors[anchors[:, 0].argsort()]
    print("k-means clustering pascal anchor points (original coordinates) \
    \nFound at iteration {} with best average IoU: {} \
    \n\nANCHORS:\n{}".format(best_avg_iou_iteration, best_avg_iou, (anchors*feature_size).tolist()))

    return anchors*feature_size

def print_anchors (anchors, dataset_folder, wi=416, hi=416, DIV=32):
  #print (wi, hi)
  img = Image.new('RGB', (wi, hi), color = 'white')
  d = ImageDraw.Draw(img)

  for a in anchors:

    w,h = a[0]*DIV, a[1]*DIV
    #print (w, h)
    #print (a[0], a[1])
    
    #print (DIV)
    shape = [(wi/2-w/2, hi/2-h/2), (wi/2+w/2, hi/2+h/2)]


    d.rectangle(shape, outline ="blue") 
    d.text((wi/2-w/2, hi/2-h/2-10), "{:0.2f}, {:0.2f}".format(a[0],a[1]), fill='blue') 

  img.show()
  img.save(os.path.join("./datasets", dataset_folder, 'anchors.png'))



