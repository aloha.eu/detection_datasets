from pycocotools.coco import COCO
import json
import os
import shutil
from uuid import uuid4
import random
import numpy as np
from utils import *

#based on the COCO dataset https://cocodataset.org/#download



src_images_folder = "/home/gderiu/detection_datasets/COCO/val2017"
annFile='/home/gderiu/detection_datasets/COCO/annotations/instances_val2017.json'



MAX_SAMPLES = 25 # the maximum size of the dataset

AND      = False # choose whether each image must contain all the objects (True) or just any of them (False)
x1y1x2y2 = True  # choose whether the bounding boxes must be espressed as xywh (False) or x1y1x2y2 (True)

# ANCHORS parameters
K = 5 # num of anchors
W, H = 416, 416
DIV = 32

BALANCE = False

#VOC_CLASSES=["airplane", "bicycle", "bird", "boat", "bottle", "bus", "car",
#             "cat", "chair", "cow", "dining table", "dog", "horse", "motorcycle",
#             "person", "potted plant", "sheep", "couch", "train", "tv"]

catNms=["person", "car"]



if __name__ == "__main__":

  classes = '_'.join(catNms)
  dataset_folder = "COCO_extract_{}_{}".format(str(uuid4()).split("-")[0], classes)
  #print ("Dataset folder: ", dataset_folder)
  dst_images_folder = os.path.join("./datasets", dataset_folder, "images")
  source_path = 'images/'
  coco=COCO(annFile)

  cats = coco.loadCats(coco.getCatIds())
  nms=[cat['name'] for cat in cats]
  print('COCO categories: \n{}\n'.format(' '.join(nms)))

  nms = set([cat['supercategory'] for cat in cats])
  print('COCO supercategories: \n{}'.format(' '.join(nms)))

  # set the categories you want to query
  catIds = coco.getCatIds(catNms=catNms)

  print ("\nRemapping classes")
  remapped_classes = {}
  for i, c in enumerate(catIds):
    categ = coco.cats.get(c)['name']
    index = catNms.index(categ)
    print ("{}: {} -> {}".format(categ,c,index))
    remapped_classes[c]=index

  if AND:
    print ("Images must contain at least an object from each class")
    imgIds = coco.getImgIds(catIds=catIds )
  else:
    print ("Images must contain at least an object from any class")
    imgIds = []
    for c in catIds:
   #   if BALANCE:
   #     imgIds += coco.getImgIds(catIds=c )[:int(MAX_SAMPLES/len(catNms))]
   #   else:
      imgIds += coco.getImgIds(catIds=c )


  imgIds=list(set(imgIds)) #unique items


  random.shuffle(imgIds)
 # imgIds=imgIds[:MAX_SAMPLES]
  #print (imgIds)

  anns_for_anchors = []
  frames= []
  
  classes_count ={}
  for c in range(len(remapped_classes)):
    classes_count[c]=0
  for i in imgIds:
    img = coco.loadImgs(i)[0]
    #print (img)
   # exit()
    annIds = coco.getAnnIds(imgIds=img['id'], catIds=catIds, iscrowd=None)
    anns = coco.loadAnns(annIds)
    objects = []
    for a in anns:
       
      bb = convert_coco_bbox((img['width'], img['height']), a['bbox'])
      anns_for_anchors.append(bb[2:])
      
      if x1y1x2y2:
        a['bbox'][2] += a['bbox'][0]
        a['bbox'][3] += a['bbox'][1]
      
      o = {'object_class' : remapped_classes[a['category_id']],
           'bb'           : a['bbox'] ,
           'polygon'      : a['segmentation']}
      classes_count[remapped_classes[a['category_id']]] += 1
      objects.append(o)
    
    frame = {"frame"   : img['file_name'],
             "width"   : img['width'],
             "height"  : img['height'],
             "mask"    : "",
             "objects" : objects}
   
    #print (frame['frame'])
    if os.path.exists(os.path.join(src_images_folder,frame['frame'])):
      frames.append(frame)

  frames = frames [:MAX_SAMPLES]

  anns_for_anchors = np.array(anns_for_anchors)
  
  centroids = anns_for_anchors[np.random.choice(np.arange(len(anns_for_anchors)), K, replace=False)]
  #centroids = anns_for_anchors[:K]
  coco_anchors = kmeans_iou(K, centroids, anns_for_anchors, feature_size= W / DIV)
  
  print ("\nSaving images...")

  dataset={"mask_path": "", "name": "tst_dataset", "source_path": source_path, "source": "image", "width": 0, "height": 0, "meta":frames}

  os.makedirs(dst_images_folder, exist_ok=True)
  for i in dataset['meta']:
    shutil.copy(os.path.join(src_images_folder,i['frame']), os.path.join(dst_images_folder,i['frame']))

  with open(os.path.join("./datasets", dataset_folder,"annotations.json"), "w") as text_file:
      text_file.write(json.dumps(dataset, indent=2, sort_keys=True))

  print_anchors(coco_anchors, dataset_folder, W, H, DIV)
  with open(os.path.join("./datasets", dataset_folder,"anchors.txt"), "w") as text_file:
      ca = coco_anchors.tolist()
      for l in ca:
        text_file.write(str(l[0]) + ', ' + str(l[1]) + '\n')
  print ("\n\nFound {} images correspondig to categories: ".format(len(frames)))
  print (catNms)
  print (classes_count)
  print ("Dataset folder: ", os.path.join("./datasets", dataset_folder))
