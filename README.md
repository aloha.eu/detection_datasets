# Detection Datasets

This project contains the scripts to extract images and annotations from the most popular datasets for object detection.

## COCO Dataset extract

Extract of the COCO dataset https://cocodataset.org/#download and a script to create custom datasets.  
The script will create also a set of anchors using k-means clustering algorithm.

## TL;DR
Just run  
```
python3 extract_from_COCO
```
You'll get a folder in the datasets directory containing a subset of the dataset, an annotations file and a jpg image with the shapes of the best anchors.  
The anchors are also reported in the console.

## Custom extract
Edit the script `extract_from_COCO.py`:
 * specify the classes from the available set;
 * specify the maximum number of samples
 * select a pair of source folder and annotation file. You can download them from here https://cocodataset.org/#download
 
Run the script.
